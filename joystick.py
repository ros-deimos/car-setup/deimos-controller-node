#!/usr/bin/env python

from evdev import InputDevice, categorize, ecodes

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Joy

import time
import os


###############################################################
class Joystick:
    def __init__(self):
        self.steering = 0.5
        self.throttle = 0.5
        try:
            self.gamepad = InputDevice('/dev/input/event1')
            print(self.gamepad)
            self.ready = True
        except IOError as ioex:
            print ("Gamepad /dev/input/event0:", os.strerror(ioex.errno))
            self.ready = False

        rospy.init_node('joystick_node', anonymous=True)
        self.publisher = rospy.Publisher('spider/joystick', Joy, queue_size=1)



################################################################
    def readGamepad(self):


        for event in self.gamepad.read_loop():
            btn_b = 0
            btn_x = 0

            if event.type == ecodes.EV_KEY:
              print("")
              #print("BTN --> ", event.type, event.code, event.value)

              if event.code == ecodes.BTN_B:
                print("B")
                btn_b = 1


              if event.code == ecodes.BTN_X:
                print("X")
                btn_x = 1

            if event.type == ecodes.EV_ABS:

              absevent = categorize(event)

              print(ecodes.bytype[absevent.event.type][absevent.event.code])

              if ecodes.bytype[absevent.event.type][absevent.event.code] == "ABS_X":
                self.steering = 1.0 - (absevent.event.value  / 255.0)
                print("steering:", self.steering)

              elif ecodes.bytype[absevent.event.type][absevent.event.code] == "ABS_Y":
                self.throttle = absevent.event.value / 255.0
                print("throttle:", self.throttle)

            self.publish_message(self.steering, self.throttle, btn_x, btn_b)


################################################################

    def publish_message(self, x, y, btn_x, btn_b):
        msg = Joy()
        msg.header.stamp = rospy.Time.now()
        msg.axes.append(x)
        msg.axes.append(y)
        msg.buttons.append(btn_x)
        msg.buttons.append(btn_b)
        self.publisher.publish(msg)
        print(msg)
##################################################################



#
# Main function to test the module
#
if __name__ == '__main__':
    remx = Joystick()
    remx.readGamepad()

