#!/usr/bin/env python

from Adafruit_PWM_Servo_Driver import PWM
import time


#############################################################

class Throttle(object):
  def __init__(self):
    self.THROTTLE_CHANNEL = 1
    self.NEUTRAL_POSITION = 368
    self.MAX_FORWARD = self.NEUTRAL_POSITION + 40
    self.MAX_BACKWARD = self.NEUTRAL_POSITION - 40

    self.ON_TIME = 0
    self.pwmDevice = PWM(0x70)
    self.pwmDevice.setPWMFreq(60)

#############################################################

  def update(self, throttle):
    pwm = self.MAX_BACKWARD + (int) (throttle * (self.MAX_FORWARD - self.MAX_BACKWARD))
    print("throttle: ", throttle, "pwm: ", pwm)
    self.pwmDevice.setPWM(self.THROTTLE_CHANNEL, 0, pwm)


#############################################################

  def updatePWM(self, pwm):
    print("pwm: ", pwm)
    self.pwmDevice.setPWM(self.THROTTLE_CHANNEL, 0, pwm)


###############################################################
  def test(self):
    for i in range(300, 1000):
      self.updatePWM(i)
      time.sleep(0.1)



#
# Main function to test the module
#
if __name__ == '__main__':
   thx = Throttle()
   thx.test()
