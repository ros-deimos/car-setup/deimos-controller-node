#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

import time
import os

class joy_driver_deimos:
    def __init__(self):
        self.steering = 0.5
        self.throttle = 0.5
        rospy.init_node('joy_driver_deimos_node', anonymous=True)
        self.publisher = rospy.Publisher('deimos/joystick', Joy, queue_size=1)

    def callback(self, msg):
        #print msg.axes[3]
        self.steering = round(msg.axes[3], 2) * 300
        self.throttle = round(msg.axes[1], 2) * 700
        print("steering:", self.steering)
        print("throttle:", self.throttle)
        self.publish_message(self.steering, self.throttle)

    def start(self):
        rospy.init_node('joy_deimos', anonymous=True)
        joy_subscriber = rospy.Subscriber("joy", Joy, self.callback, queue_size=10)
        rospy.spin()

    def publish_message(self, x, y):
        msg = Joy()
        msg.header.stamp = rospy.Time.now()
        msg.axes.append(x)
        msg.axes.append(y)
        self.publisher.publish(msg)
        print(msg)
#
# Main function to test the module
#
if __name__ == '__main__':
    joy = joy_driver_deimos()
    joy.start()
