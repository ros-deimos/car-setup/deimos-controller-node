import picamera
import time
import os
from config import Config
import socket
import subprocess
import io
from picamera.array import PiRGBArray

########################################################################

class Camera:
  def __init__(self):
    self.ready = True
    self.camera  = picamera.PiCamera()
    self.camera.framerate = 20

    self.camera.vflip = True
    self.camera.hflip = True
    #self.camera.iso = 100
    #self.camera.brightness = 60
    self.camera.resolution = (480, 320)
    #self.camera.color_effects = (128, 128) # black and white
    #self.camera.annotate_text = 'SpiderX'
    self.config = Config()
    self.rawImage = PiRGBArray(self.camera, size=self.camera.resolution)
    self.stream = self.camera.capture_continuous(self.rawImage, format="rgb", use_video_port=True)
    time.sleep(2)
    self.on = True
########################################################################


  def capture(self, imageName, nginxPath):
    imgPath = self.config.DATA_OUTPUT_PATH + "/" + imageName
    self.camera.capture(imgPath, resize=(640, 420))
    os.system("cp " + imgPath + " " + nginxPath)


########################################################################

  def start_sequence_one(self):
    start = time.time()
    for filename in self.camera.capture_continuous("../data/" + '{counter:03d}.rgb', format="rgb", use_video_port=True):
      #os.system("cp " + filename + " /var/www/html/img/camera.rgb")
      print('Captured %s' % filename)
      print(time.time() - start)
      start = time.time()
      #time.sleep(0.02)

########################################################################


  def start_sequence_two(self):
    print("sequence two")
    start = time.time()
    for f in self.stream:
      # grab the frame from the stream and clear the stream in
      # preparation for the next frame
      self.frame = f.array
      self.rawImage.truncate(0)

      # if the thread indicator variable is set, stop the thread
      if not self.on:
         break
      print("update frame...")
      print(time.time() - start)
      start = time.time()


cam = Camera()
# cam.capture('000.jpg')
# cam.start_streaming()
cam.start_sequence_two()
