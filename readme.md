# Setup tools for self driving car

## Download the raspbian OS and create a SD card
- Download the Raspbian iso image https://www.raspberrypi.org/downloads/raspbian/
- Install Etcher tool from https://etcher.io/
- Flash the sd card with the downloaded iso image
- Atfer flashing the SD card add ../wpa_supplicant.conf file with your credentials to the root folder of the SD card


## Setup wifi connection

the wifi connection configuration file is: /etc/wpa_supplicant/wpa_supplicant.conf

## updating the rspi
```
$ sudo rpi-update # firmware update
$ sudo apt-get update && sudo apt-get upgrade  # os update
$  uname -a
```

## Setup Bonjour protocol
```
$ sudo apt-get install avahi-daemon libavahi-client-dev
$ sudo update-rc.d avahi-daemon defaults
$ sudo nano /etc/avahi/services/ssh.service
```
with the following content

```
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
<name replace-wildcards="yes">%h SSH</name>
<service>
<type>_ssh._tcp</type>
<port>22</port>
</service>
</service-group>
```

## Coonect to the raspi
```
$ ssh-keygen -R deimos-car.local
$ ssh deimos@deimos-car.local
```


## Configure bluetooth on raspi
```
$ sudo apt-get install pi-bluetooth
$ systemctl status bluetooth
$ hcitool scan

$ bluetoothctl
$ power on
$ scan on
$ agent on
$ pair 18:7A:93:48:AC:F9
$ trust 18:7A:93:48:AC:F9
$ exit
$ ls /dev/input
$ cat /dev/input/event0  (change event0 to eventX if needed)

```
## Using the bluetooth joystick in python
```
$ sudo apt-get install python3-pip
$ sudo pip install evdev
$ pip3 install evdev

```

## Use the PWM rasberry hat module
> https://github.com/adafruit/Adafruit_Python_PCA9685
```
$ sudo raspi-config  # (then enable I2C in the interfacing options )
$ sudo apt-get install python-smbus
$ sudo i2cdetect -y 1  (to test the new configuration)
```

## Use Adafruit_PWM_Servo_Driver module
```
$ git clone -b legacy https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code.git

cd  cd Adafruit-Raspberry-Pi-Python-Code/
cd Adafruit_PWM_Servo_Driver/
nano Servo_Example.py

```

## Enable stream camera on the topic /image_raw_compressed
```
$ sudo raspi-config  # and enable the camera and IC2
$ roslaunch usb_cam usb_cam-test.launch
$ rosrun rqt_image_view rqt_image_view

Record with rosbag ...
$

```

## Restore image boot on SDcard
Then use the this to write the image back to the SD card:
```
$ cd images/
$ sudo dd if=deimos_car_data.dmg of=/dev/disk2
```

## Launch ROS Master server on deimos-car
```
$ source ~/catkin_ws/devel/setup.bash
$ roscore &
```

## Resolve host ip deimos-car and Set ROS_MASTER_URI in env linux bash client VM
Then use the this to write the image back to the SD card:
```
$ avahi-resolve -n -4 deimos-car.local
$ export ROS_MASTER_URI=http://deimos-car.local:11311
```

## Stop the raspi
```
sudo halt
poweroff
```
