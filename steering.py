#!/usr/bin/env python

from Adafruit_PWM_Servo_Driver import PWM
import time


#############################################################
class Steering(object):
  def __init__(self):

    self.remoteActive = True
    self.pwmDevice = PWM(0x40)
    self.pwmDevice.setPWMFreq(50)
    self.STEERING_CHANNEL = 0
    self.on_time = 0

    self.MAX_LEFT_PWM = 490
    self.MAX_RIGHT_PWM = 270


###############################################################
  def update(self, steering):
   pwm =  self.MAX_RIGHT_PWM  + (int)( steering  * (self.MAX_LEFT_PWM - self.MAX_RIGHT_PWM ))
   self.pwmDevice.setPWM(self.STEERING_CHANNEL, 0, pwm)
   print("steering: ", steering, "pwm: ", pwm)

  def updatePWM(self, pwm):
   self.pwmDevice.setPWM(self.STEERING_CHANNEL, 0, pwm)
   print("steering: ", pwm)

###############################################################
  def test(self):
    for i in range(0, 100):
      self.update(i / 100.0)
      time.sleep(0.1)


#
# Main function to test the module
#
if __name__ == '__main__':
   stx = Steering()
   stx.test()

