#!/usr/bin/env python

import time
import sys
import rospy
from std_msgs.msg import Int16
from steering import Steering
from throttle import Throttle

########################################################################

class Deimos:
    def __init__(self):
        self.steering = Steering()
        self.throttle = Throttle()
        self.steering.updatePWM(322)
        self.throttle.updatePWM(360)

    def callback_steering(self, data):
        self.update_steering(data)

    def callback_speed(self, data):
        self.update_speed(data)
   

    def run_listener(self):
        rospy.init_node('deimos_node', anonymous=True)
        rospy.Subscriber("/pwm/speed", Int16, self.callback_speed)
        rospy.Subscriber("/pwm/steering", Int16, self.callback_steering)
        rospy.spin()

    def start(self):
        self.run_listener()

    def update_speed(self, data):
        print("speed: ", data)
        self.throttle.updatePWM(data.data)

    def update_steering(self, data):
        print("steering: ", data)
        self.steering.updatePWM(data.data)

#
# Main function to test the module
#
if __name__ == '__main__':
    remx = Deimos()
    remx.start()
